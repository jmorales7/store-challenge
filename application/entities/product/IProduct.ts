import { PaginatedResult, PaginationParams } from '../common/Pagination';
import { Product } from './Product';
import { CreateProductDTO, UpdateProductDTO } from './Product.dto';

export type IProduct = {
  create(params: CreateProductDTO, sellerId: string): Promise<Product>;
  getById(productId: string): Promise<Product>;
  update(productId: string, params: UpdateProductDTO): Promise<Product>;
  delete(productId: string): Promise<Product>;
  getAll(params: PaginationParams, name: string, sku: string, range: any): Promise<PaginatedResult<Product>>;
};
