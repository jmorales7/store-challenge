export interface Product {
  id: string;
  name: string;
  sku: string;
  quantity: number;
  price: number;
  sellerId: string;
  createdAt: string;
  updatedAt: string;
}
