import { PaginatedResult } from '../common/Pagination';
import { Product } from './Product';
import { CreateProductDTO, UpdateProductDTO } from './Product.dto';

export type IRepositoryProduct = {
  create(params: CreateProductDTO, sellerId: string): Promise<Product>;
  getById(productId: string): Promise<Product>;
  update(productId: string, params: UpdateProductDTO): Promise<Product>;
  delete(productId: string): Promise<Product>;
  getAll(params: any): Promise<PaginatedResult<Product>>;
};
