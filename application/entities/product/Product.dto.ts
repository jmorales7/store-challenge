import { z } from 'zod';

export const CreateProductDTO = z
  .object({
    name: z.string(),
    sku: z.string(),
    quantity: z.number(),
    price: z.number()
  })
  .strict();

export type CreateProductDTO = z.infer<typeof CreateProductDTO>;

export const UpdateProductDTO = z
  .object({
    productId: z.string(),
    name: z.string().optional(),
    sku: z.string().optional(),
    quantity: z.number().optional(),
    price: z.number().optional()
  })
  .strict();

export type UpdateProductDTO = z.infer<typeof UpdateProductDTO>;

export const GetIdProductDTO = z
  .object({
    productId: z.string()
  })
  .strict();

export type GetIdProductDTO = z.infer<typeof GetIdProductDTO>;

export const GetAllProductDTO = z
  .object({
    limit: z.string().nonempty().regex(/^\d+$/, 'Invalid, need to be a number').transform(Number),
    page: z.string().nonempty().regex(/^\d+$/, 'Invalid, need to be a number').transform(Number),
    search: z.string().optional(),
    name: z.string().optional(),
    sku: z.string().optional(),
    to: z.string().regex(/^\d+$/, 'Invalid, need to be a number').transform(Number).optional(),
    from: z.string().regex(/^\d+$/, 'Invalid, need to be a number').transform(Number).optional()
  })
  .strict();
export type GetAllProductDTO = z.infer<typeof GetAllProductDTO>;
