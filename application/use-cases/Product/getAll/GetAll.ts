import { IServices } from 'infrastructure/services/IServices';
import { ILogger } from 'logger/Logger';

import { CustomError, Errors } from '../../../entities/shared/Errors';
import { PaginatedResult } from '../../../entities/common/Pagination';
import { Product } from '../../../entities/product/Product';
import { UserRole } from '../../../entities/user/User';

type Payload = {
  limit: number;
  search: string;
  page: number;
  name: string;
  sku: string;
  from: number;
  to: number;
  identificatorObject: any;
};
export const getAllProduct =
  ({ productService, authService }: IServices, logger: ILogger) =>
  async ({
    limit,
    search,
    page,
    name,
    sku,
    from,
    to,
    identificatorObject
  }: Payload): Promise<PaginatedResult<Product>> => {
    logger.info('UOC-getAllProduct');
    const { hasPermission, user } = await authService.identify(
      identificatorObject.authorization?.split(' ')[1],
      'getAllProduct'
    );
    if (!hasPermission) throw new CustomError(Errors.BAD_CREDENTIALS, 'Bad permissions');
    const range = { from, to };
    if (user.role === UserRole.enum.ADMIN)
      return await productService.getAll({ page, limit, search }, name, sku, range);
    return await productService.getAll({ page, limit, search: user.id }, name, sku, range);
  };
