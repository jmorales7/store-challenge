import { IServices } from 'infrastructure/services/IServices';
import { ILogger } from 'logger/Logger';

import { CustomError, Errors } from '../../../entities/shared/Errors';
import { CreateProductDTO } from '../../../entities/product/Product.dto';
import { Product } from '../../../entities/product/Product';
type Payload = {
  body: CreateProductDTO;
  identificatorObject: any;
};
export const createProduct =
  ({ productService, authService }: IServices, logger: ILogger) =>
  async ({ body, identificatorObject }: Payload): Promise<Product> => {
    logger.info('UOC-createProduct');
    const { hasPermission, user } = await authService.identify(
      identificatorObject.authorization?.split(' ')[1],
      'createProduct'
    );
    if (!hasPermission) throw new CustomError(Errors.BAD_CREDENTIALS, 'Bad permissions');
    return await productService.create(body, user.id);
  };
