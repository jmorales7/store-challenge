import { IServices } from 'infrastructure/services/IServices';
import { ILogger } from 'logger/Logger';

import { CustomError, Errors } from '../../../entities/shared/Errors';
import { Product } from '../../../entities/product/Product';

type Payload = {
  productId: string;
  identificatorObject: any;
};
export const getByIdProduct =
  ({ productService, authService }: IServices, logger: ILogger) =>
  async ({ productId, identificatorObject }: Payload): Promise<Product> => {
    logger.info('UOC-getByIdProduct');
    const { hasPermission } = await authService.identify(
      identificatorObject.authorization?.split(' ')[1],
      'getByIdProduct'
    );
    if (!hasPermission) throw new CustomError(Errors.BAD_CREDENTIALS, 'Bad permissions');
    return await productService.getById(productId);
  };
