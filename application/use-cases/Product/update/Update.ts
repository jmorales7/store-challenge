import { IServices } from 'infrastructure/services/IServices';
import { ILogger } from 'logger/Logger';

import { CustomError, Errors } from '../../../entities/shared/Errors';
import { Product } from '../../../entities/product/Product';
import { UpdateProductDTO } from '../../../entities/product/Product.dto';

type Payload = {
  productId: string;
  body: UpdateProductDTO;
  identificatorObject: any;
};
export const updateProduct =
  ({ productService, authService }: IServices, logger: ILogger) =>
  async ({ productId, body, identificatorObject }: Payload): Promise<Product> => {
    logger.info('UOC-updateProduct');
    const { hasPermission } = await authService.identify(
      identificatorObject.authorization?.split(' ')[1],
      'updateProduct'
    );
    if (!hasPermission) throw new CustomError(Errors.BAD_CREDENTIALS, 'Bad permissions');
    return await productService.update(productId, body);
  };
