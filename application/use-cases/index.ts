import { IServices } from 'infrastructure/services/IServices';

import { ILogger } from '../../logger/Logger';
import { createAuth } from './Auth/create/Create';
import { getByRole } from './Auth/getByRole/GetByRole';
import { updateAuth } from './Auth/update/Update';
import { createUser } from './User/create/Create';
import { signIn } from './User/signIn/SignIn';
import { createProduct } from './Product/create/Create';
import { deleteProduct } from './Product/delete/Delete';

import { updateProduct } from './Product/update/Update';

import { getByIdProduct } from './Product/getById/GetById';

import { getAllProduct } from './Product/getAll/GetAll';

export const useCaseFactory = (services: IServices, baseLogger: ILogger) => ({
  createAuth: createAuth(services, baseLogger.child({ controller: 'createAuth' })),
  getByRole: getByRole(services, baseLogger.child({ controller: 'getByRole' })),
  updateAuth: updateAuth(services, baseLogger.child({ controller: 'updateAuth' })),
  createUser: createUser(services, baseLogger.child({ controller: 'createUser' })),
  signIn: signIn(services, baseLogger.child({ controller: 'signIn' })),
  createProduct: createProduct(services, baseLogger.child({ controller: 'createProduct' })),
  deleteProduct: deleteProduct(services, baseLogger.child({ controller: 'deleteProduct' })),
  updateProduct: updateProduct(services, baseLogger.child({ controller: 'updateProduct' })),
  getByIdProduct: getByIdProduct(services, baseLogger.child({ controller: 'getByIdProduct' })),
  getAllProduct: getAllProduct(services, baseLogger.child({ controller: 'getAllProduct' }))
});
