import { IServices } from 'infrastructure/services/IServices';
import { ILogger } from 'logger/Logger';

import { CustomError, Errors } from '../../../entities/shared/Errors';
import { User } from '../../../entities/user/User';
import { CreateUserDTO } from '../../../entities/user/User.dto';

type Payload = {
  body: CreateUserDTO;
  identificatorObject: any;
};
export const createUser =
  ({ userService, authService }: IServices, logger: ILogger) =>
  async ({ body }: Payload): Promise<User> => {
    logger.info('UOC-createUser');

    const user = await userService.getByEmail(body.email);
    if (user) throw new CustomError(Errors.ALREADY_EXIST, `Already exist ${body.email}`);
    const password = body.password;
    await userService.create(body);
    return await userService.signIn(password, body.email);
  };
