import { IRepositoryProduct } from 'application/entities/product/Product.repository';
import { IRepositoryUser } from '../../application/entities/user/User.repository';
import { IAuth } from './auth/IAuth';
export type IDataAccess = {
  authModule: IAuth;
  userRepository: IRepositoryUser;
  productRepository: IRepositoryProduct;
};
