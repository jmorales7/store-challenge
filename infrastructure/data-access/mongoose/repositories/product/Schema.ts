import { Document, Schema, model } from 'mongoose';

import { PaginatedResult } from '../../../../../application/entities/common/Pagination';
import { Product } from 'application/entities/product/Product';

// Document interface
export interface ProductDocument extends Product, Document {
  id: string;
}

// Schema
const schema = new Schema<ProductDocument>(
  {
    name: {
      type: String
    },
    sku: {
      type: String
    },
    quantity: {
      type: Number
    },
    price: {
      type: Number
    },
    sellerId: {
      type: String
    }
  },
  { timestamps: {} }
);

export const ProductModel = model<ProductDocument>('Product', schema);

export const productDataParser = (product: ProductDocument): Product => {
  return {
    id: product._id.toString(),
    name: product.name,
    sku: product.sku,
    quantity: product.quantity,
    price: product.price,
    sellerId: product.sellerId,
    updatedAt: product.createdAt,
    createdAt: product.updatedAt
  };
};

export const parsePaginateData = (paginateData: PaginatedResult<ProductDocument>): PaginatedResult<Product> => ({
  page: paginateData.page || 1,
  limit: paginateData.limit,
  pages: paginateData.pages,
  total: paginateData.total,
  results: paginateData.results.map((productDocument: ProductDocument) => productDataParser(productDocument)!)
});
