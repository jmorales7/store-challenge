import { ProductModel, productDataParser, parsePaginateData } from './Schema';
import { ILogger } from 'logger/Logger';
import { CustomError, Errors } from '../../../../../application/entities/shared/Errors';
import { IRepositoryProduct } from 'application/entities/product/Product.repository';
import { paginate } from '../../pagination/Pagination';
import { Aggregate } from 'mongoose';

export const productRepository = (logger: ILogger): IRepositoryProduct => ({
  async create(product, sellerId) {
    const log = logger.child({ function: 'create' });
    try {
      return ProductModel.create({ sellerId, ...product }).then((product) => {
        return productDataParser(product)!;
      });
    } catch (e: any) {
      log.error(e);
      throw new CustomError(Errors.SERVER_ERROR, e);
    }
  },
  async getById(id) {
    const log = logger.child({ function: 'getById' });
    try {
      const product = await ProductModel.findOne({ _id: id });
      if (!product) throw new CustomError(Errors.NOT_FOUND, 'Not Found');
      return productDataParser(product);
    } catch (e: any) {
      log.error(e);
      throw new CustomError(Errors.SERVER_ERROR, e);
    }
  },
  async update(productId, product) {
    const log = logger.child({ function: 'update' });
    try {
      return await ProductModel.findOneAndUpdate({ _id: productId }, product, { new: true, omitUndefined: true }).then(
        (product: any) => {
          if (!product) throw new CustomError(Errors.NOT_FOUND, 'Product not found');
          return productDataParser(product);
        }
      );
    } catch (e: any) {
      log.error(e);
      throw new CustomError(Errors.SERVER_ERROR, e);
    }
  },
  async delete(productId) {
    const log = logger.child({ function: 'delete' });
    try {
      return await ProductModel.findOneAndDelete({ _id: productId }).then((product: any) => {
        if (!product) throw new CustomError(Errors.NOT_FOUND, 'Product not found');
        return product;
      });
    } catch (e: any) {
      log.error(e);
      throw new CustomError(Errors.SERVER_ERROR, e);
    }
  },
  async getAll({ aggregationPipeline, options }) {
    const log = logger.child({ function: 'getAll' });
    try {
      const pipeline: Aggregate<any[]> = ProductModel.aggregate(aggregationPipeline);
      const products = await paginate(ProductModel, pipeline, options);
      return parsePaginateData(products);
    } catch (e: any) {
      log.error(e);
      throw new CustomError(Errors.SERVER_ERROR, e);
    }
  }
});
