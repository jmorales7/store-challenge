import { AnyZodObject } from 'zod';
import { CreateAuthDTO, GetByRoleDTO, UpdateAuthDTO } from '../../../../application/entities/auth/Auth.dto';

import { CreateUserDTO } from '../../../../application/entities/user/User.dto';
import {
  CreateProductDTO,
  GetAllProductDTO,
  GetIdProductDTO,
  UpdateProductDTO
} from '../../../../application/entities/product/Product.dto';

export type Route = {
  path: string;
  verb: string;
  useCase: any;
  successCode?: number;
  fileBuffer?: boolean;
  schemaValidation?: AnyZodObject | undefined;
};

export const routes: (dependencies: any) => Array<Route> = (dependencies: any) => [
  //AUTH
  { path: '/auth', verb: 'POST', useCase: dependencies.createAuth, schemaValidation: CreateAuthDTO },
  { path: '/auth/:role', verb: 'GET', useCase: dependencies.getByRole, schemaValidation: GetByRoleDTO },
  { path: '/auth/:role', verb: 'PUT', useCase: dependencies.updateAuth, schemaValidation: UpdateAuthDTO },
  //USERS
  { path: '/users', verb: 'POST', useCase: dependencies.createUser, schemaValidation: CreateUserDTO },
  { path: '/sign-in', verb: 'POST', useCase: dependencies.signIn },
  //PRODUCTS
  { path: '/products', verb: 'POST', useCase: dependencies.createProduct, schemaValidation: CreateProductDTO },
  {
    path: '/products/:productId',
    verb: 'GET',
    useCase: dependencies.getByIdProduct,
    schemaValidation: GetIdProductDTO
  },
  {
    path: '/products/:productId',
    verb: 'PUT',
    useCase: dependencies.updateProduct,
    schemaValidation: UpdateProductDTO
  },
  {
    path: '/products/:productId',
    verb: 'DELETE',
    useCase: dependencies.deleteProduct,
    schemaValidation: GetIdProductDTO
  },
  { path: '/products', verb: 'GET', useCase: dependencies.getAllProduct, schemaValidation: GetAllProductDTO }
];
