import { IProduct } from '../../../application/entities/product/IProduct';
import { ILogger } from '../../../logger/Logger';
import { IDataAccess } from '../../data-access/IDataAccess';
import { searchRegex } from '../../data-access/mongoose/helper';

export const productService = ({ productRepository }: IDataAccess, logger: ILogger): IProduct => ({
  async create(product, sellerId) {
    return await productRepository.create(product, sellerId);
  },
  async getById(id) {
    return await productRepository.getById(id);
  },
  async update(id, params) {
    return await productRepository.update(id, params);
  },
  async getAll({ page, limit, search }, name, sku, range) {
    const parsedSearch = searchRegex(search ?? '');
    const options = {
      page,
      limit,
      sort: { createdAt: 1 }
    };
    const filter: any = [];
    if (name) {
      filter.push({ name: { $regex: searchRegex(name), $options: 'i' } });
    }
    if (sku) {
      filter.push({ sku: { $regex: searchRegex(sku), $options: 'i' } });
    }
    if (range && range.from && range.to) {
      filter.push({ price: { $gte: Number(range.from), $lte: Number(range.to) } });
    }

    const aggregationPipeline = [
      {
        $match: {
          $and: filter.length === 0 ? [{}] : filter,
          $or: [{ sellerId: { $regex: parsedSearch, $options: 'i' } }]
        }
      }
    ];
    return await productRepository.getAll({ aggregationPipeline, options });
  },

  async delete(productId) {
    return await productRepository.delete(productId);
  }
});
