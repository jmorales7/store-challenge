import { IAuthEntity } from '../../application/entities/auth/IAuth';
import { IUser } from '../../application/entities/user/IUser';
import { IProduct } from '../../application/entities/product/IProduct';

export type IServices = {
  authService: IAuthEntity;
  userService: IUser;
  productService: IProduct;
};
