# Clean Architecture Authentication Project

This is an authentication project that utilizes clean architecture to maintain modular, flexible, and easily maintainable code. The project uses the following technologies:

- TypeScript: A typed programming language that compiles to JavaScript.
- Node.js: A JavaScript runtime environment for server-side execution.
- Express.js: A web framework for Node.js that facilitates the creation of web applications and APIs.
- MongoDB: A NoSQL database used for storing project data.
- JWT (JSON Web Tokens): An open standard based on JSON for creating access tokens used for authenticating and authorizing HTTP requests.
- Prettier: A code formatting tool that helps maintain consistent style throughout the project.
- Zod: A schema library for data validation in TypeScript.
- Bcrypt: A library for secure password hashing and verification.

## Advantages of Using Clean Architecture

Clean Architecture offers several advantages in application development, some of which are highlighted below:

1. Separation of Concerns: The architecture is divided into layers, allowing clear separation of concerns and facilitating maintenance and system evolution.

2. Framework and Library Independence: Architecture layers are decoupled from implementation details, enabling changing or updating technologies without affecting other parts of the system.

3. Unit Testing: The modular structure and separation of concerns make it easier to write unit tests, ensuring code quality and system robustness.

4. Flexibility and Scalability: Clean architecture enables efficient system scaling, whether through adding new features or managing high workloads.

5. Long-Term Maintainability: The clear and modular structure of the code facilitates long-term understanding and maintenance, reducing technical debt accumulation and improving code readability.

# API Routes

Below are the available routes in this API along with their corresponding HTTP verbs and functionalities.

## AUTH

- `POST /auth`: Create a new authentication in the database.
- `GET /auth/:role`: Get authentication by role.
- `PUT /auth/:role`: Update authentication by role.

## USERS

- `POST /users`: Create a new user in the database.
- `POST /sign-in`: Sign in a user.

## PRODUCTS

- `POST /products`: Create a new product in the database.
- `GET /products/:productId`: Get a product by its ID.
- `PUT /products/:productId`: Update a product by its ID.
- `DELETE /products/:productId`: Delete a product by its ID.
- `GET /products`: Get all available products.

## Validation Schemas

Each route has its corresponding validation schema specified as `schemaValidation`. Validation schemas are used to ensure that the data received in requests meets certain requirements before being processed by the API.


## Getting Started

To set up the project locally using Yarn, follow these steps:

1. Install the dependencies:

yarn install


2. Start the development server using nodemon:

yarn dev


This will run the API server using nodemon, which automatically restarts the server when changes are detected in the code. You can access the API at `http://localhost:<PORT>`, where `<PORT>` is the port number specified in your server configuration.

## Environment Variables

Please note that the API may require additional configurations or environment variables to run correctly

The API requires the following environment variables to be set:

- `HTTP_PORT`: The port number on which the API will run.
- `LOG_LEVEL`: The log level for the API (e.g., `debug`, `info`, `error`, etc.).
- `MONGO_URI`: The connection URI for MongoDB.
- `SECRET_JWT`: The secret key used for JWT token generation and validation.
- `SALT_ROUNDS`: The number of salt rounds for password hashing.

Ensure that you set these environment variables correctly before running the API. You can do this by creating a `.env` file in the root directory of the project and adding the key-value pairs:

Happy coding!


Enjoy the clean architecture authentication project! Feel free to explore and expand the project according to your needs.
